<!--
Software Name : NIF TZ
SPDX-FileCopyrightText: Copyright (c) Orange SA
SPDX-License-Identifier: Apache-2.0

This software is distributed under the  Apache-2.0
See the "LICENSES" directory for more details.

Authors:
- Sylvain Desbureaux <sylvain.desbureaux@orange.com>
- Morgan Richomme <morgan.richomme@orange.com>
-->

# CloudNativePG release retrieval

This project automatically retrieve CloudNativePG Operator GitHub tags and
create a tag with them in `./base`.

It then add custom overrides in `./overlays/releases/ha/application` and
`./overlays/releases/lite/application`.

The deployment can be deployed in 2 flavors:

* `ha` : deployment with 3 replicas (HA) when applicable
* `lite` : deployment with only one replica.

The flavor choice is done by setting the cluster variable `DEPLOYMENT_MODE` to
`ha` (default) or `lite`.
